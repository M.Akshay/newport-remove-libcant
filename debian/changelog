newport (0.2020.1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to target

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 28 Feb 2021 08:28:17 +0000

newport (0.2020.1) apertis; urgency=medium

  * Switch to native format to work with the GitLab-to-OBS pipeline.
  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Tue, 14 Jan 2020 11:17:48 +0800

newport (0.1706.2-0co4) apertis; urgency=medium

  * debian/control: Depend on the hotdoc-0.8 branch

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 08 Jan 2020 16:02:10 +0000

newport (0.1706.2-0co3) apertis; urgency=medium

  * AppArmor: Allow access gvfs

 -- Frédéric Danis <frederic.danis@collabora.com>  Thu, 19 Dec 2019 09:08:14 +0100

newport (0.1706.2-0co2) apertis; urgency=medium

  * AppArmor: Reload profiles on installation (Apertis: T5294)

 -- Frédéric Danis <frederic.danis@collabora.com>  Tue, 30 Jul 2019 13:55:14 +0200

newport (0.1706.2-0co1) apertis; urgency=medium

  * Fix build with curl versions defining CURLE_SSL_CACERT as
    CURLE_PEER_FAILED_VERIFICATION.
  * Fix gbp.conf.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Tue, 02 Apr 2019 10:06:07 +0200

newport (0.1706.1-0co1) 17.06; urgency=medium

  [ Rohan Madgula ]
  * Set up a local server to handle file download (Apertis: T2832)
  * Re-check network connectivity when connman Proxy is changed
  * Set proxy for curl if any

  [ Justin Kim ]
  * Add autocleanup to DBus code generation flag

  [ Simon McVittie ]
  * tests/: Append to CLEANFILES from glib-tap.mk, don't override
  * Packaging improvements:
    - Remove newport-dbg, switch to -dbgsym packages (Apertis: T3142)
    - Do not build-depend on unused dh-systemd
    - Move to debhelper compat level 10
    - debian/.gitignore: Add
    - Split out libnewportiface0 binary package (part of T1643)
    - Normalize packaging (wrap-and-sort -abst)
    - Require complete symbols files if not UNRELEASED
  * AppArmor fixes:
    - Inline the parts of chaiwala-base that Newport uses (Apertis: T3999)
    - Rely on base abstraction to provide Journal logging
    - Restrict system bus communication
    - Lock down D-Bus access to Newport a bit more (Apertis: T3995)

 -- Simon McVittie <smcv@collabora.com>  Wed, 24 May 2017 16:42:34 +0100

newport (0.1706.0-0co1) 17.06; urgency=medium

  [ Rohan Madgula ]
  * Remove unwanted support of GTestDbus (Apertis: T3833)

  [ Simon McVittie ]
  * AppArmor: Allow logging to the Journal via .../socket
  * Stop depending on transitional canterbury-dev package

 -- Simon McVittie <smcv@collabora.com>  Fri, 21 Apr 2017 17:15:59 +0100

newport (0.1703.0-0co1) 17.03; urgency=medium

  [ Emanuele Aina ]
  * tests: Clean up test names

  [ Frédéric Dalleau ]
  * debian: Depends on libglib2.0-bin

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Fri, 03 Mar 2017 08:43:39 +0000

newport (0.1612.1-0co1) 16.12; urgency=medium

  [ Mathieu Duponchelle ]
  * Require hotdoc version 0.8
  * docs/reference/Makefile.am: use --gdbus-codegen-sources.

  [ Guillaume Desmottes ]
  * ship D-Bus service file (Apertis: T3091)
  * apparmor: stop using -strict abstractions

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Thu, 15 Dec 2016 12:37:17 +0000

newport (0.1612.0-0co1) 16.12; urgency=medium

  [ Héctor Orón Martínez ]
  * debian/gbp.conf: extend default attributes and add dch

  [ Rohan Madgula ]
  * Integrated TAP test driver for installed test support (Apertis: T2733)
  * Download path and apparmor validation is done through "CbyProcessInfo"

  [ Simon McVittie ]
  * AppArmor: update for new path of newport-client
  * AppArmor: allow logging to Journal stdout socket
  * newport-client AppArmor: update profile so it works again

  [ Rohan Madgula ]
  * Set libexecdir to /usr/lib
  * Add autopkgtest tests

  [ Simon McVittie ]
  * Add debian/source/apertis-component marking this as a target package

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Tue, 25 Oct 2016 16:11:31 +0200

newport (0.2.7-0co1) 16.09; urgency=medium

  [ Mathieu Duponchelle ]
  * Documentation: stop using gtk-doc
  * debian: Remove doc-base file.
  * git.mk: update to upstream master
  * configure.ac: enable silent rules
  * documentation: port to hotdoc 0.8
  * newport-errors.h: Add missing glib.h include

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Thu, 15 Sep 2016 23:09:12 +0200

newport (0.2.6-0co1) 16.06; urgency=medium

  [ Rohan Madgula ]
  * Include <abstractions/chaiwala-base> instead of <abstractions/base>
  * All variable declarations moved to top.
  * resolved "-Wshadow" warning in newport-service-handler.c
  * resolved "-Wswitch-enum" errors in newport.

  [ Simon McVittie ]
  * packaging: accept any implementation of libcurl

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Mon, 27 Jun 2016 15:26:08 +0100

newport (0.2.5-0co1) 16.03; urgency=medium

  [ Rohan Madgula ]
  * apparmor profile corrected for newport service

  [ Sjoerd Simons ]
  * Release 0.2.5

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 14 Apr 2016 10:35:32 +0200

newport (0.2.4-0co1) 16.03; urgency=medium

  [ Rohan Madgula ]
  * Added missing statement of marking the current state as NEWPORT_DOWNLOAD_FAILED when newport fails to create file for download.
  * Removed unnecessary addition of download file name to the group name as download path includes the download file name.
  * Added addtional check for string length in group name creation as this is used for name of temporary download file.
  * removed uinProgressCount and uinSpeedLimit variables from DownloadMgr structure,and these values are read during runtime of newport.
  * apparmor profile corrected for newport and newport-client.
  * database path for storing download information updated.

  [ Sjoerd Simons ]
  * newport: Fix enum name

  [ Rohan Madgula ]
  * Added the missing apparmor profile for newport-client

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Tue, 22 Mar 2016 13:32:41 +0100

newport (0.2.3-0co1) 16.03; urgency=medium

  [ Aleksander Morgado ]
  * interface: register public enum types in dbus library
  * interface: add introspection support
  * interface: register error domain in dbus
  * build: rebuild gtk-doc sections always

  [ Sjoerd Simons ]
  * Use the generic GNOME autogen.sh

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Mon, 29 Feb 2016 21:52:32 +0100

newport (0.2.2-0co1) 16.03; urgency=medium

  [ rohan ]
  * beckfoot dependency is removed and GNetworkMonitor is used instead.
  * network preference management added and connman service is used instead
  * namespace changes to "apertis" done.
  * removed inclusion of "dbus/dbus-glib.h" header as it is not required.
  * GKeyFile is used instead of libseaton as a backend for storing
    persistent data

  [ Dilip Balaraju ]
  * removed ribchester dependency from newport
  * Design structure changed as per the reviews.
  * libseaton dependency updated with version.
  * Added GLib message logging mechanism.
  * g_return_val_if_fail() and g_return_if_fail() methods used where ever it
    is necessary.
  * test client updated by adding g_test utility for testing unit test cases.

  [ Simon McVittie ]
  * Clean up build system so we can auto-build from git (T770):
    - Use a static README file and the standard Autotools INSTALL
    - Do not explicitly distribute files that are always distributed anyway
    - Clean up generated files correctly
    - Use AX_COMPILER_FLAGS for compiler warnings instead of reinventing it
    - Correctly include git.mk in every Makefile.am
    - Do not remove gtk-doc.make in distclean
    - debian/rules: use autogen.sh for autoreconf, so we gtkdocize
    - .arcconfig: add
    - Fail the build if anything is not installed
    - Remove undesired dh_shlibdeps override
  * Replace Newport schema with a desktop file (T572)
    - Don't make unnecessary Autoconf substitutions in o.a.Newport.Config
      schema

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Thu, 07 Jan 2016 15:03:53 +0000

newport (0.2.1-0co1) 15.09; urgency=medium

  [ Philip Withnall ]
  * src: Use rename() instead of system("mv …") to rename downloaded files

  [ Simon McVittie ]
  * debian/source/options: ignore git.mk and .arcconfig in the Debian
    diff, since these are not intended to appear in tarballs but may
    appear in git
  * debian/usr.bin.newport: allow connecting to system bus
  * debian/usr.bin.newport: allow username and hostname lookup
  * debian/usr.bin.newport: allow access to
    /var/lib/SAC_Services/Download-Manager/app-data
  * Fix out-of-tree builds
  * Specify an in-prefix systemdunitdir to fix distcheck
  * newport.service is built during build, so clean it in clean,
    not maintainer-clean
  * debian/source/options: ignore ChangeLog in the debian diff, since
    it is generated in tarballs
  * debian/postinst: add #DEBHELPER# token so that debhelper-generated code
    can work
  * debian/usr.bin.newport: allow communication with ribchester;
    Newport reads properties from
    /org/secure_automotive_cloud/Ribchester/AppStore

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Fri, 21 Aug 2015 18:30:52 +0100

newport (0.2.0-0rb2) 15.03; urgency=low

  * Initial import

 -- Apertis packagers  <packagers@lists.apertis.org>  Thu, 12 Mar 2015 16:10:40 +0530
