/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "newport-internal.h"
#include <errno.h>
#include <glib/gstdio.h>
#include <fcntl.h>

static gint fs_truncate(FILE* file, gint64 size);

static FILE*
curl_plugin_open_file (DownloadUrlInfo* pUrlInfo,GError **error);

/*as GKeyFile is used as backend for storing download info which is not thread safe,so all database operations are performed in main thread itself*/
static gboolean
store_url_information_in_default_main_context (gpointer user_data)
{
  GError *error=NULL;
  DownloadUrlInfo* url_info = user_data;
  if (!store_url_data_in_db (url_info, &error))
     {
       NEWPORT_WARNING("%s", error->message);
       g_clear_pointer(&error,g_error_free);
     }
  return G_SOURCE_REMOVE;
}


// libcurl progress callback function
static gint download_progress_clb(DownloadUrlInfo* pUrlInfo, double dltotal,
		double dlnow, double ultotal, double ulnow)
{

  gdouble dDownloadSpeed;
  gdouble dElapsedTime;
  guint progress_count;
  GDBusObject *dbus_object;
  NewportDownload *Download;

  progress_count = g_settings_get_uint(pDnlMgrObj.configuration_schema,"emit-progress-count");
  // get file size
  pUrlInfo->progress.u64TotalSize = (gint64) pUrlInfo->FileOffsetBeginning
      + (guint64) dltotal;
  dbus_object = newport_download_url_info_ensure_dbus_object (pUrlInfo);
  Download = (NewportDownload *) g_dbus_object_get_interface (dbus_object,
                                                              "org.apertis.Newport.Download");

  curl_easy_getinfo (pUrlInfo->pCurlObg, CURLINFO_TOTAL_TIME, &dElapsedTime);
  curl_easy_getinfo (pUrlInfo->pCurlObg, CURLINFO_SPEED_DOWNLOAD,
                     &dDownloadSpeed);

  // if total size is unknown, make pUrlInfo->progress.u64TotalSize zero which indicates unknown size.
  if ((guint64) dlnow > pUrlInfo->progress.u64TotalSize)
    pUrlInfo->progress.u64TotalSize = 0;

  newport_download_set_total_size (Download, pUrlInfo->progress.u64TotalSize);
  pUrlInfo->progress.u64CurrentSize = pUrlInfo->FileOffsetBeginning
      + (guint64) dlnow;
  newport_download_set_downloaded_size (Download,
                                        pUrlInfo->progress.u64CurrentSize);

    pUrlInfo->progress.u64RemainingTime = (pUrlInfo->progress.u64TotalSize
        - pUrlInfo->progress.u64CurrentSize) / dDownloadSpeed;

  pUrlInfo->progress.u64DownloadSpeed = (guint64) dDownloadSpeed;
  pUrlInfo->progress.u64ElapsedTime = pUrlInfo->uin64ElapsedTimeBeg
      + (guint64) dElapsedTime;

  // if user has paused the download then get out of the thread
  if ((pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_PAUSED_BY_USER)
      || (pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_CANCELLED)
      || (pUrlInfo->u32CurrentDownloadState == NEWPORT_DOWNLOAD_STATE_PAUSED_BY_SYSTEM))
    {
      NEWPORT_DEBUG("Stopped the curl download thread %s",
          pUrlInfo->pDownloadFileName);
      pUrlInfo->progress.u8ProgressCount = 0;
      g_object_unref (Download);
      g_clear_object(&dbus_object);
      return 1;
    }

  // progress count
  pUrlInfo->progress.u8ProgressCount++;
  if (pUrlInfo->progress.u8ProgressCount == progress_count)
    {
      pUrlInfo->progress.u8ProgressCount = 0;
      if ((pUrlInfo->pAppName) && ((guint64) dltotal))
        {
          send_progress_update_to_client (Download, pUrlInfo);
        }
    }
  g_object_unref (Download);
  g_clear_object(&dbus_object);
  return 0;
}

static FILE* curl_plugin_open_file(DownloadUrlInfo* pUrlInfo,GError **error)
{
  FILE* fp;
  gchar *group_name=newport_download_get_group_name(pUrlInfo);
  pUrlInfo->pTempDownloadPath = newport_build_temporary_download_path(group_name);
  g_free(group_name);

  /* this should take care of starting of resuming the download */
  if ((fp = fopen (pUrlInfo->pTempDownloadPath, "ab+")) == NULL)
    {
      g_set_error (error, NEWPORT_DOWNLOAD_MANAGER_ERROR,
                   NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED, "%s",
                   "could not open the file to download");
      return fp;
    }

  fseek (fp, 0, SEEK_END);
  pUrlInfo->FileOffset = ftell (fp);
  NEWPORT_DEBUG("File offset %"G_GOFFSET_FORMAT"", pUrlInfo->FileOffset);
  curl_easy_setopt (pUrlInfo->pCurlObg, CURLOPT_WRITEDATA, fp);
  curl_easy_setopt (pUrlInfo->pCurlObg, CURLOPT_RESUME_FROM_LARGE,
                    pUrlInfo->FileOffset);

	return fp;
}

/*This task is run in thread and operations on  pUrlInfo are controlled from main thread, and sufficient conditions are taken care that pUrlInfo information is modified from main thread and current thread and no other thread can modify this information*/
void
start_new_download_thread (GTask *task, NewportDownload *Download,
                           DownloadUrlInfo *pUrlInfo, GCancellable *cancellable)
{
	GError *err = NULL;
	CURL* pCurlObg;
	FILE *fp;
	CURLcode curl_code;
	gint error_no=-1;
//	long	curl_time;
	long response_code;
	guint speed_limit;
  DownloadUrlInfo *url_info_copy;
  GProxyResolver *proxy_resolver = NULL;
  g_auto (GStrv) proxy_uris = NULL;
  gchar **proxy_uri = NULL;

  g_return_if_fail(pDnlMgrObj.uinDownloadCount > 0);
  speed_limit = g_settings_get_uint(pDnlMgrObj.configuration_schema,"max-download-speed");
  NEWPORT_DEBUG("Starting new download thread, Count = %d",
      pDnlMgrObj.uinDownloadCount);

  // pCurlObg init
  if (pUrlInfo->pCurlObg == NULL)
    pUrlInfo->pCurlObg = curl_easy_init ();
  pCurlObg = pUrlInfo->pCurlObg;
  curl_code = CURLE_FAILED_INIT;

  fp = curl_plugin_open_file (pUrlInfo, &err);
  if (err)
    {
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      newport_download_set_error (Download,
                                  NEWPORT_DOWNLOAD_MANAGER_ERROR_FAILED);
      g_task_return_error (task, err);
        pDnlMgrObj.uinDownloadCount--;
      return;
    }

  proxy_resolver = g_proxy_resolver_get_default ();

  if (proxy_resolver != NULL)
    proxy_uris = g_proxy_resolver_lookup (proxy_resolver, pUrlInfo->pUrl, NULL,
                                          NULL);
  // Url for the download
  curl_easy_setopt (pCurlObg, CURLOPT_URL, pUrlInfo->pUrl);
  curl_easy_setopt (pCurlObg, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_easy_setopt (pCurlObg, CURLOPT_SSL_VERIFYHOST, 2);
  curl_easy_setopt (pCurlObg, CURLOPT_FILE, fp);

  /* set the proxy URI for curl other than "direct://" */
  for (proxy_uri = proxy_uris; proxy_uri != NULL && *proxy_uri != NULL; proxy_uri++)
    {
      NEWPORT_DEBUG ("proxy uri is %s", *proxy_uri);
      /* if proxy_uri is "direct://" It means there is no proxy configured
       * in the system so ignore it */
      if (g_strcmp0 (*proxy_uri, "direct://"))
        curl_easy_setopt (pCurlObg, CURLOPT_PROXY, *proxy_uri);
    }
  // Set the maximum speed limit for the file to be downloaded
  curl_easy_setopt (pCurlObg, CURLOPT_MAX_RECV_SPEED_LARGE,
                    (curl_off_t) speed_limit);

  // use libcurl's redirection.
  curl_easy_setopt (pCurlObg, CURLOPT_FOLLOWLOCATION, 1);
  curl_easy_setopt (pCurlObg, CURLOPT_AUTOREFERER, 1);
  curl_easy_setopt (pCurlObg, CURLOPT_MAXREDIRS, 30);

  // Progress  --------------------------------------------------------------
  curl_easy_setopt (pCurlObg, CURLOPT_PROGRESSFUNCTION,
                     download_progress_clb);
  curl_easy_setopt (pCurlObg, CURLOPT_PROGRESSDATA, pUrlInfo);
  curl_easy_setopt (pCurlObg, CURLOPT_NOPROGRESS, FALSE);

  // Others -----------------------------------------------------------------
  curl_easy_setopt (pCurlObg, CURLOPT_FILETIME, 1);
  curl_easy_setopt (pCurlObg, CURLOPT_ERRORBUFFER, pUrlInfo->pCurlErrorString);

  pUrlInfo->FileOffsetBeginning = pUrlInfo->FileOffset;

  // perform
  curl_code = curl_easy_perform (pCurlObg);

  // get size of output file
  fseek (fp, 0, SEEK_END);
  pUrlInfo->OldFileOffset = pUrlInfo->FileOffset;
  pUrlInfo->FileOffset = ftell (fp);
  // server response code
  response_code = 0;
  curl_easy_getinfo (pCurlObg, CURLINFO_RESPONSE_CODE, &response_code);

  // HTTP response code
  // 4xx Client Error
  // 5xx Server Error
  if (response_code >= 400)
    {
      NEWPORT_DEBUG("Server response code : %ld", response_code);
      // discard data if error
      pUrlInfo->FileOffset = pUrlInfo->OldFileOffset;
      fseek (fp, pUrlInfo->FileOffset, SEEK_SET);
      fs_truncate (fp, pUrlInfo->FileOffset);
      // Don't post completed message.
      curl_code = CURLE_ABORTED_BY_CALLBACK;
    }

  // curl_easy_perform() returned code.
  /* This switch statement each case in different manner based on curl_codes
   * if curl_code is CURLE_OK it returns task value as TRUE
   * if curl_code is CURLE_ABORTED_BY_CALLBACK it returns task value as FALSE
   * if curl_code is CURLE_RANGE_ERROR or CURLE_BAD_DOWNLOAD_RESUME temporary file will be deleted and same function is called recursively.
   * in rest all other cases error_no is set based on curl_code.
   * */
  switch (curl_code)
    {
    case CURLE_OK:
      NEWPORT_DEBUG("CURL DOWNLOAD STATUS: OK ");
      curl_easy_getinfo (pCurlObg, CURLINFO_FILETIME, &pUrlInfo->lCurlTime);
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_SUCCESS;
      g_task_return_boolean (task, TRUE);
      break;

      // can resume (retry)
    case CURLE_PARTIAL_FILE:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      break;

      // can't resume (retry)
    case CURLE_RANGE_ERROR:
    case CURLE_BAD_DOWNLOAD_RESUME:
      // discard data
      pUrlInfo->FileOffset = 0;
      fseek (fp, 0, SEEK_SET);
      fs_truncate (fp, 0);
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_IN_PROGRESS;
      newport_download_set_state (Download, NEWPORT_DOWNLOAD_STATE_IN_PROGRESS);
      NEWPORT_WARNING("CURL DNL STATUS: Range error or bad download error");
      unlink_file (pUrlInfo->pTempDownloadPath);
      url_info_copy = download_url_info_copy (pUrlInfo);
      /*as GKeyFile is used as backend for storing download info which is not thread safe,so all database operations are performed in main thread itself*/
      g_idle_add_full (G_PRIORITY_HIGH_IDLE,
                       store_url_information_in_default_main_context,
                       url_info_copy, (GDestroyNotify) download_url_info_free);
      start_new_download_thread (task, Download, pUrlInfo, cancellable);
      /* send a download failure message */
      break;

      // retry
    case CURLE_GOT_NOTHING:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      break;
    case CURLE_RECV_ERROR:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      break;
    case CURLE_OPERATION_TIMEDOUT:
    case CURLE_BAD_CONTENT_ENCODING:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      /* send a retry message to resume the download */
      break;

      // can't connect (retry)
    case CURLE_COULDNT_CONNECT:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      break;

      // too many redirection (exit)
    case CURLE_TOO_MANY_REDIRECTS:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      break;

      // abort by user (exit)
    case CURLE_ABORTED_BY_CALLBACK:
      NEWPORT_DEBUG("CURL DNL STATUS: pCurlObg was aborted by the user callback");
      g_task_return_boolean (task, FALSE);
      break;

      // out of resource (exit)
    case CURLE_OUT_OF_MEMORY:
    case CURLE_WRITE_ERROR:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NO_MEMORY;
      break;

      // exit
    case CURLE_UNSUPPORTED_PROTOCOL:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      break;

      // other error (exit)
    case CURLE_COULDNT_RESOLVE_HOST:
    case CURLE_COULDNT_RESOLVE_PROXY:
    case CURLE_FAILED_INIT:
    case CURLE_URL_MALFORMAT:
    case CURLE_FTP_WEIRD_SERVER_REPLY:
    case CURLE_REMOTE_ACCESS_DENIED:
    case CURLE_FTP_ACCEPT_FAILED:
    case CURLE_FTP_WEIRD_PASS_REPLY:
    case CURLE_FTP_ACCEPT_TIMEOUT:
    case CURLE_FTP_WEIRD_PASV_REPLY:
    case CURLE_FTP_WEIRD_227_FORMAT:
    case CURLE_FTP_CANT_GET_HOST:
    case CURLE_HTTP2:
    case CURLE_FTP_COULDNT_SET_TYPE:
    case CURLE_FTP_COULDNT_RETR_FILE:
    case CURLE_OBSOLETE20:
    case CURLE_QUOTE_ERROR:
    case CURLE_HTTP_RETURNED_ERROR:
    case CURLE_OBSOLETE24:
    case CURLE_UPLOAD_FAILED:
    case CURLE_READ_ERROR:
    case CURLE_OBSOLETE29:
    case CURLE_FTP_PORT_FAILED:
    case CURLE_FTP_COULDNT_USE_REST:
    case CURLE_OBSOLETE32:
    case CURLE_HTTP_POST_ERROR:
    case CURLE_SSL_CONNECT_ERROR:
    case CURLE_FILE_COULDNT_READ_FILE:
    case CURLE_LDAP_CANNOT_BIND:
    case CURLE_LDAP_SEARCH_FAILED:
    case CURLE_OBSOLETE40:
    case CURLE_FUNCTION_NOT_FOUND:
    case CURLE_BAD_FUNCTION_ARGUMENT:
    case CURLE_OBSOLETE44:
    case CURLE_INTERFACE_FAILED:
    case CURLE_OBSOLETE46:
    case CURLE_UNKNOWN_OPTION:
    case CURLE_TELNET_OPTION_SYNTAX:
    case CURLE_OBSOLETE50:
    case CURLE_PEER_FAILED_VERIFICATION:
    case CURLE_SSL_ENGINE_NOTFOUND:
    case CURLE_SSL_ENGINE_SETFAILED:
    case CURLE_SEND_ERROR:
    case CURLE_OBSOLETE57:
    case CURLE_SSL_CERTPROBLEM:
    case CURLE_SSL_CIPHER:
#if CURLE_SSL_CACERT != CURLE_PEER_FAILED_VERIFICATION
    case CURLE_SSL_CACERT:
#endif
    case CURLE_LDAP_INVALID_URL:
    case CURLE_FILESIZE_EXCEEDED:
    case CURLE_USE_SSL_FAILED:
    case CURLE_SEND_FAIL_REWIND:
    case CURLE_SSL_ENGINE_INITFAILED:
    case CURLE_LOGIN_DENIED:
    case CURLE_TFTP_NOTFOUND:
    case CURLE_TFTP_PERM:
    case CURLE_REMOTE_DISK_FULL:
    case CURLE_TFTP_ILLEGAL:
    case CURLE_TFTP_UNKNOWNID:
    case CURLE_REMOTE_FILE_EXISTS:
    case CURLE_TFTP_NOSUCHUSER:
    case CURLE_CONV_FAILED:
    case CURLE_CONV_REQD:
    case CURLE_SSL_CACERT_BADFILE:
    case CURLE_REMOTE_FILE_NOT_FOUND:
    case CURLE_SSH:
    case CURLE_SSL_SHUTDOWN_FAILED:
    case CURLE_AGAIN:
    case CURLE_SSL_CRL_BADFILE:
    case CURLE_SSL_ISSUER_ERROR:
    case CURLE_FTP_PRET_FAILED:
    case CURLE_RTSP_CSEQ_ERROR:
    case CURLE_RTSP_SESSION_ERROR:
    case CURLE_FTP_BAD_FILE_LIST:
    case CURLE_CHUNK_FAILED:
    case CURLE_NO_CONNECTION_AVAILABLE:
    case CURLE_SSL_PINNEDPUBKEYNOTMATCH:
    case CURLE_SSL_INVALIDCERTSTATUS:
    case CURLE_NOT_BUILT_IN:
    case CURL_LAST:
    default:
      pUrlInfo->u32CurrentDownloadState = NEWPORT_DOWNLOAD_STATE_FAILED;
      error_no=NEWPORT_DOWNLOAD_MANAGER_ERROR_NETWORK;
      break;
	}

  if (error_no != -1)
    {
      newport_download_set_error (Download, error_no);
      g_set_error (&err, NEWPORT_DOWNLOAD_MANAGER_ERROR, error_no, "%s",
                   pUrlInfo->pCurlErrorString);
      g_task_return_error (task, err);
    }
	if (fp)
	{
      fclose (fp);
      fp = NULL;
	}

		pDnlMgrObj.uinDownloadCount--;

}

void download_url_info_free(DownloadUrlInfo *pUrlInfo)
{
  g_return_if_fail (pUrlInfo != NULL);
      g_free (pUrlInfo->pAppName);
      g_free (pUrlInfo->pUrl);
      g_free (pUrlInfo->pDownloadPath);
      g_free (pUrlInfo->pDownloadFileName);
      g_free (pUrlInfo->object_path);
      g_free (pUrlInfo->pTempDownloadPath);
  if (pUrlInfo->pCurlObg)
    curl_easy_cleanup (pUrlInfo->pCurlObg);
  g_free (pUrlInfo);

}

gint unlink_file(const gchar *pFilename)
{
  g_return_val_if_fail (pFilename != NULL, -1);
  if (g_get_filename_charsets (NULL))
    {
      NEWPORT_DEBUG("Temp download path %s", pFilename);
      return g_unlink (pFilename);
    }
  else
    {
      gchar *cp_filename = g_filename_from_utf8 (pFilename, -1, NULL, NULL,
                                                 NULL);
      gint save_errno;
      gint retval;

      NEWPORT_DEBUG ("Temporary download path %s", cp_filename);

      if (cp_filename == NULL)
        {
          errno = EINVAL;
          return -1;
        }

      retval = g_unlink (cp_filename);
      save_errno = errno;

      g_free (cp_filename);

      errno = save_errno;
      return retval;
    }
}

static gint fs_truncate(FILE* file, gint64 size)
{
	gint fd;

	fflush(file);
	fseek(file, size, SEEK_SET);
	fd = fileno(file);
	return ftruncate(fd, size);
}
